﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmVistaArticulo_Venta : Form
    {
        public FrmVistaArticulo_Venta()
        {
            InitializeComponent();
        }

        //Método Ocultar columnas
        private void OcultarColumnas()
        {
            this.dataListado.Columns[0].Visible = false;
            this.dataListado.Columns[1].Visible = false;
        }

        //Método Buscar Nombre
        private void MostrarArticuloVentaNombre()
        {
            this.dataListado.DataSource = NVenta.MostrarArticuloVentaNombre(this.textBoxBuscar.Text);
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        private void MostrarArticuloVentaCodigo()
        {
            this.dataListado.DataSource = NVenta.MostrarArticuloVentaCodigo(this.textBoxBuscar.Text);
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        private void FrmVistaArticulo_Venta_Load(object sender, EventArgs e)
        {
            
        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            if (comboBoxBuscar.Text.Equals("CODIGO"))
            {
                this.MostrarArticuloVentaCodigo();
            }
            else if (comboBoxBuscar.Text.Equals("NOMBRE"))
            {
                this.MostrarArticuloVentaNombre();
            }
        }

        private void dataListado_DoubleClick(object sender, EventArgs e)
        {
            FrmVenta form = FrmVenta.GetInstancia();
            string par1, par2;
            decimal par3, par4;
            int par5;
            DateTime par6;
            par1 = Convert.ToString(dataListado.CurrentRow.Cells["iddetalle_ingreso"].Value);
            par2 = Convert.ToString(dataListado.CurrentRow.Cells["nombre"].Value);
            par3 = Convert.ToDecimal(dataListado.CurrentRow.Cells["precio_compra"].Value);
            par4 = Convert.ToDecimal(dataListado.CurrentRow.Cells["precio_venta"].Value);
            par5 = Convert.ToInt32(dataListado.CurrentRow.Cells["stock_actual"].Value);
            par6 = Convert.ToDateTime(dataListado.CurrentRow.Cells["fecha_vencimiento"].Value);

            form.setArticulo(par1,par2,par3,par4,par5,par6);
            Hide();
        }
    }
}
