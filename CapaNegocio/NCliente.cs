﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using CapaDatos;

namespace CapaNegocio
{
    public class NCliente
    {
        //idcliente, nombre, apellidos, sexo, fecha_nacimiento, tipo_documento, numero_documento, direccion, telefono, email
        //Método Insertar que llame al método insertar de la clase Proveedor de la capaDatos
        public static string Insertar(string nombre, string apellidos, string sexo, DateTime fechaNacimiento, string tipoDocumento, string numeroDocumento, string direccion, string telefono, string email)
        {
            DCliente Obj = new DCliente();
            Obj.Nombre = nombre;
            Obj.Apellidos = apellidos;
            Obj.Sexo = sexo;
            Obj.FechaNacimiento = fechaNacimiento;
            Obj.TipoDocumento = tipoDocumento;
            Obj.NumeroDocumento = numeroDocumento;
            Obj.Direccion = direccion;
            Obj.Telefono = telefono;
            Obj.Email = email;
            return Obj.Insertar(Obj);
        }

        //Método Editar
        public static string Editar(int idcliente, string nombre, string apellidos, string sexo, DateTime fechaNacimiento, string tipoDocumento, string numeroDocumento, string direccion, string telefono, string email)
        {
            DCliente Obj = new DCliente();
            Obj.Idcliente = idcliente;
            Obj.Nombre = nombre;
            Obj.Apellidos = apellidos;
            Obj.Sexo = sexo;
            Obj.FechaNacimiento = fechaNacimiento;
            Obj.TipoDocumento = tipoDocumento;
            Obj.NumeroDocumento = numeroDocumento;
            Obj.Direccion = direccion;
            Obj.Telefono = telefono;
            Obj.Email = email;
            return Obj.Editar(Obj);
        }

        //Método Eliminar
        public static string Eliminar(int idcliente)
        {
            DCliente Obj = new DCliente();
            Obj.Idcliente = idcliente;
            return Obj.Eliminar(Obj);
        }

        //Método Mostrar
        public static DataTable Mostrar()
        {
            return new DCliente().Mostrar();
        }

        //Método Buscar RazonSocial
        public static DataTable BuscarClienteApellidos(string textobuscar)
        {
            DCliente Obj = new DCliente();
            Obj.TextoBuscar = textobuscar;
            return Obj.BuscarClienteApellidos(Obj);
        }

        //Método Buscar NumDocumento
        public static DataTable BuscarClienteNumDocumento(string textobuscar)
        {
            DCliente Obj = new DCliente();
            Obj.TextoBuscar = textobuscar;
            return Obj.BuscarClienteNumDocumento(Obj);
        }
    }
}
