﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace CapaDatos
{
    public class DDetalle_Ingreso
    {
        //iddetalle_ingreso, idingreso, idarticulo, precio_compra, precio_venta, stock_inicial, stock_actual, fecha_produccion, fecha_vencimiento
        private int _IddetalleIngreso;
        private int _Idingreso;
        private int _Idarticulo;
        private decimal _PrecioCompra;
        private decimal _PrecioVenta;
        private int _StockInicial;
        private int _StockActual;
        private DateTime _FechaProduccion;
        private DateTime _FechaVencimiento;

        public int IddetalleIngreso
        {
            get { return _IddetalleIngreso; }
            set { _IddetalleIngreso = value; }
        }

        public int Idingreso
        {
            get { return _Idingreso; }
            set { _Idingreso = value; }
        }

        public int Idarticulo
        {
            get { return _Idarticulo; }
            set { _Idarticulo = value; }
        }

        public decimal PrecioCompra
        {
            get { return _PrecioCompra; }
            set { _PrecioCompra = value; }
        }

        public decimal PrecioVenta
        {
            get { return _PrecioVenta; }
            set { _PrecioVenta = value; }
        }

        public int StockInicial
        {
            get { return _StockInicial; }
            set { _StockInicial = value; }
        }

        public int StockActual
        {
            get { return _StockActual; }
            set { _StockActual = value; }
        }

        public DateTime FechaProduccion
        {
            get { return _FechaProduccion; }
            set { _FechaProduccion = value; }
        }

        public DateTime FechaVencimiento
        {
            get { return _FechaVencimiento; }
            set { _FechaVencimiento = value; }
        }

        public DDetalle_Ingreso()
        {
            
        }

        public DDetalle_Ingreso(int iddetalleIngreso, int idingreso, int idarticulo, decimal precioCompra, decimal precioVenta, int stockInicial, int stockActual, DateTime fechaProduccion, DateTime fechaVencimiento)
        {
            IddetalleIngreso = iddetalleIngreso;
            Idingreso = idingreso;
            Idarticulo = idarticulo;
            PrecioCompra = precioCompra;
            PrecioVenta = precioVenta;
            StockInicial = stockInicial;
            StockActual = stockActual;
            FechaProduccion = fechaProduccion;
            FechaVencimiento = fechaVencimiento;
        }

        public string Insertar(DDetalle_Ingreso DetalleIngreso,
            ref SqlConnection SqlCon, ref SqlTransaction SqlTra)
        {
            string rpta = "";
            try
            {
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.Transaction = SqlTra;
                SqlCmd.CommandText = "spinsertar_detalle_ingreso";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParIddetalleIngreso = new SqlParameter();
                ParIddetalleIngreso.ParameterName = "@iddetalle_ingreso";
                ParIddetalleIngreso.SqlDbType = SqlDbType.Int;
                ParIddetalleIngreso.Direction = ParameterDirection.Output;
                SqlCmd.Parameters.Add(ParIddetalleIngreso);

                SqlParameter ParIdingreso = new SqlParameter();
                ParIdingreso.ParameterName = "@idingreso";
                ParIdingreso.SqlDbType = SqlDbType.Int;
                ParIdingreso.Value = DetalleIngreso.Idingreso;
                SqlCmd.Parameters.Add(ParIdingreso);

                SqlParameter ParIdarticulo = new SqlParameter();
                ParIdarticulo.ParameterName = "@idarticulo";
                ParIdarticulo.SqlDbType = SqlDbType.Int;
                ParIdarticulo.Value = DetalleIngreso.Idarticulo;
                SqlCmd.Parameters.Add(ParIdarticulo);

                SqlParameter ParPrecioCompra = new SqlParameter();
                ParPrecioCompra.ParameterName = "@precio_compra";
                ParPrecioCompra.SqlDbType = SqlDbType.Money;
                ParPrecioCompra.Value = DetalleIngreso.PrecioCompra;
                SqlCmd.Parameters.Add(ParPrecioCompra);

                SqlParameter ParPrecioVenta = new SqlParameter();
                ParPrecioVenta.ParameterName = "@precio_venta";
                ParPrecioVenta.SqlDbType = SqlDbType.Money;
                ParPrecioVenta.Value = DetalleIngreso.PrecioVenta;
                SqlCmd.Parameters.Add(ParPrecioVenta);

                SqlParameter ParStockInicial = new SqlParameter();
                ParStockInicial.ParameterName = "@stock_inicial";
                ParStockInicial.SqlDbType = SqlDbType.Int;
                ParStockInicial.Value = DetalleIngreso.StockInicial;
                SqlCmd.Parameters.Add(ParStockInicial);

                SqlParameter ParStockActual = new SqlParameter();
                ParStockActual.ParameterName = "@stock_actual";
                ParStockActual.SqlDbType = SqlDbType.Int;
                ParStockActual.Value = DetalleIngreso.StockActual;
                SqlCmd.Parameters.Add(ParStockActual);

                SqlParameter ParFechaProduccion = new SqlParameter();
                ParFechaProduccion.ParameterName = "@fecha_produccion";
                ParFechaProduccion.SqlDbType = SqlDbType.Date;
                ParFechaProduccion.Value = DetalleIngreso.FechaProduccion;
                SqlCmd.Parameters.Add(ParFechaProduccion);

                SqlParameter ParFechaVencimiento = new SqlParameter();
                ParFechaVencimiento.ParameterName = "@fecha_vencimiento";
                ParFechaVencimiento.SqlDbType = SqlDbType.Date;
                ParFechaVencimiento.Value = DetalleIngreso.FechaVencimiento;
                SqlCmd.Parameters.Add(ParFechaVencimiento);

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO SE INGRESO EL REGISTRO";

            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            
            return rpta;
        }
    }
}
