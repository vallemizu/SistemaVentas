﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaPresentacion.Consultas;

namespace CapaPresentacion
{
    public partial class FrmPrincipal : Form
    {
        private int childFormNumber = 0;

        public string Idtrabajador = "";
        public string Apellidos = "";
        public string Nombre = "";
        public string Acceso = "";

        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void categoriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCategoria frm = new FrmCategoria();
            frm.MdiParent = this;
            frm.Show();
        }

        private void presentacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPresentacion frm = new FrmPresentacion();
            frm.MdiParent = this;
            frm.Show();
        }

        private void articulosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmArticulo frm = FrmArticulo.GetInstancia();
            frm.MdiParent = this;
            frm.Show();
        }

        private void proveedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmProveedor frm = new FrmProveedor();
            frm.MdiParent = this;
            frm.Show();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCliente frm = new FrmCliente();
            frm.MdiParent = this;
            frm.Show();
        }

        private void trabajadoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTrabajador frm = new FrmTrabajador();
            frm.MdiParent = this;
            frm.Show();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            this.GestionUsuario();
            nombreLabel.Text = Nombre;
            apellidosLabel.Text = Apellidos;
            accesoLabel.Text = Acceso;
        }

        private void GestionUsuario()
        {
            //Controla los accesos
            if (Acceso == "ADMINISTRADOR")
            {
                this.almacenTSM.Enabled = true;
                this.comprasTSM.Enabled = true;
                this.ventaTSM.Enabled = true;
                this.mantenimientoTSM.Enabled = true;
                this.consultasTSM.Enabled = true;
                this.herramientasTSM.Enabled = true;
                this.articulosTSB.Enabled = true;
                categoriasTSB.Enabled = true;
                presentacionTSB.Enabled = true;
                ingresosTSB.Enabled = true;
                proveedoresTSB.Enabled = true;
                ventasTSB.Enabled = true;
                clientesTSB.Enabled = true;
                trabajadorTSB.Enabled = true;
            }
            //Controla los accesos
            else if (Acceso == "VENDEDOR")
            {
                this.almacenTSM.Enabled = false;
                this.comprasTSM.Enabled = false;
                this.ventaTSM.Enabled = true;
                this.mantenimientoTSM.Enabled = false;
                this.consultasTSM.Enabled = true;
                this.herramientasTSM.Enabled = false;
                this.articulosTSB.Enabled = false;
                categoriasTSB.Enabled = false;
                presentacionTSB.Enabled = false;
                ingresosTSB.Enabled = false;
                proveedoresTSB.Enabled = false;
                ventasTSB.Enabled = true;
                clientesTSB.Enabled = true;
                trabajadorTSB.Enabled = false;
            }
            else if (Acceso == "ALMACENERO")
            {
                this.almacenTSM.Enabled = true;
                this.comprasTSM.Enabled = true;
                this.ventaTSM.Enabled = false;
                this.mantenimientoTSM.Enabled = false;
                this.consultasTSM.Enabled = true;
                this.herramientasTSM.Enabled = true;
                this.articulosTSB.Enabled = true;
                categoriasTSB.Enabled = true;
                presentacionTSB.Enabled = true;
                ingresosTSB.Enabled = true;
                proveedoresTSB.Enabled = true;
                ventasTSB.Enabled = false;
                clientesTSB.Enabled = false;
                trabajadorTSB.Enabled = false;
            }

            else
            {
                this.almacenTSM.Enabled = false;
                this.comprasTSM.Enabled = false;
                this.ventaTSM.Enabled = false;
                this.mantenimientoTSM.Enabled = false;
                this.consultasTSM.Enabled = false;
                this.herramientasTSM.Enabled = false;
                this.articulosTSB.Enabled = false;
                categoriasTSB.Enabled = false;
                presentacionTSB.Enabled = false;
                ingresosTSB.Enabled = false;
                proveedoresTSB.Enabled = false;
                ventasTSB.Enabled = false;
                clientesTSB.Enabled = false;
                trabajadorTSB.Enabled = false;
            }
        }

        private void FrmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void articulosTSB_Click(object sender, EventArgs e)
        {
            articulosToolStripMenuItem_Click(sender, e);
        }

        private void categoriasTSB_Click(object sender, EventArgs e)
        {
            categoriasToolStripMenuItem_Click(sender, e);
        }

        private void presentacionTSB_Click(object sender, EventArgs e)
        {
            presentacionToolStripMenuItem_Click(sender, e);
        }

        private void ingresosTSM_Click(object sender, EventArgs e)
        {
            FrmIngreso frm=FrmIngreso.GetInstancia();
            frm.MdiParent = this;
            frm.Show();
            frm.idtrabajador = Convert.ToInt32(this.Idtrabajador);
        }

        private void proveedoresTSB_Click(object sender, EventArgs e)
        {
            proveedoresToolStripMenuItem_Click(sender, e);
        }

        private void ventasTSM_Click(object sender, EventArgs e)
        {
            FrmVenta frm=FrmVenta.GetInstancia();
            frm.MdiParent = this;
            frm.Show();
            frm.idtrabajador = Convert.ToInt32(this.Idtrabajador);
        }

        private void clientesTSB_Click(object sender, EventArgs e)
        {
            clientesToolStripMenuItem_Click(sender, e);
        }

        private void trabajadorTSB_Click(object sender, EventArgs e)
        {
            trabajadoresToolStripMenuItem_Click(sender, e);
        }

        private void ingresosTSB_Click(object sender, EventArgs e)
        {
            ingresosTSM_Click(sender, e);
        }

        private void ventasTSB_Click(object sender, EventArgs e)
        {
            ventasTSM_Click(sender, e);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            salirToolStripMenuItem_Click(sender, e);
        }

        private void stockDeArticulosTSM_Click(object sender, EventArgs e)
        {
            Consultas.FrmConsultaStockArticulos frm= new Consultas.FrmConsultaStockArticulos();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
