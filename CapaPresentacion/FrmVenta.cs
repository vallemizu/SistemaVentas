﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmVenta : Form
    {
        private bool IsNuevo = false;
        public int idtrabajador;
        private DataTable dtDetalle;

        private decimal totalPagado = 0;

        private static FrmVenta _instancia;

        public static FrmVenta GetInstancia()
        {
            if (_instancia == null)
            {
                _instancia = new FrmVenta();
            }
            return _instancia;
        }

        Convertir convertir = new Convertir();

        public void setCliente(string idcliente, string nombre)
        {
            this.textBoxIdCliente.Text = idcliente;
            this.textBoxCliente.Text = nombre;
        }

        public void setArticulo(
            string iddetalle_ingreso, 
            string nombre, 
            decimal precio_compra,
            decimal precio_venta, 
            int stock, 
            DateTime fecha_vencimiento)
        {
            this.textBoxIdArticulo.Text = iddetalle_ingreso;
            this.textBoxArticulo.Text = nombre;
            this.textBoxPrecioCompra.Text = Convert.ToString(precio_compra);
            this.textBoxPrecioVenta.Text = Convert.ToString(precio_venta);
            this.textBoxStockActual.Text = Convert.ToString(stock);
            this.dateTimeFechaVencimiento.Value = fecha_vencimiento;
        }

        public FrmVenta()
        {
            InitializeComponent();
            this.ttMensaje.SetToolTip(this.textBoxCliente,"Seleccione un Cliente");
            this.ttMensaje.SetToolTip(this.textBoxSerie, "Seleccione una serie del comprobante");
            this.ttMensaje.SetToolTip(this.textBoxCorrelativo, "Ingrese un número del comprobante");
            this.ttMensaje.SetToolTip(this.textBoxCantidad, "Ingrese la cantidad del Artículo");
            this.ttMensaje.SetToolTip(this.textBoxArticulo, "Seleccione un Artículo");
            this.textBoxIdCliente.Visible = false;
            this.textBoxIdArticulo.Visible = false;
            this.textBoxCliente.ReadOnly = true;
            this.textBoxArticulo.ReadOnly = true;
            this.dateTimeFechaVencimiento.Enabled = false;
            this.textBoxPrecioCompra.ReadOnly = true;
            this.textBoxPrecioCompra.ReadOnly = true;
            this.textBoxStockActual.ReadOnly = true;
        }

        //Mostrar Mensaje de confirmación
        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema de Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema de Ventas", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //Limpiar controles
        private void Limpiar()
        {
            this.textBoxIdVenta.Text = string.Empty;
            this.textBoxIdCliente.Text = string.Empty;
            this.textBoxCliente.Text = string.Empty;
            this.textBoxSerie.Text = string.Empty;
            this.textBoxCorrelativo.Text = string.Empty;
            this.textBoxIgv.Text = string.Empty;
            //this.labelTotalPagado.Text = "0,0";
            labelTotalPagado.Text = Convertir.numeroAletras(decimal.Parse(totalPagado.ToString()));
            this.textBoxIgv.Text = "18";
            this.crearTabla();
        }

        private void limpiarDetalle()
        {
            textBoxIdArticulo.Text = string.Empty;
            textBoxArticulo.Text = string.Empty;
            textBoxStockActual.Text = string.Empty;
            textBoxCantidad.Text = string.Empty;
            textBoxPrecioCompra.Text = string.Empty;
            textBoxPrecioVenta.Text = string.Empty;
            textBoxDescuento.Text = string.Empty;
        }

        //Habilitar controles del formulario
        private void Habilitar(bool valor)
        {
            this.textBoxIdVenta.ReadOnly = !valor;
            this.textBoxSerie.ReadOnly = !valor;
            this.textBoxCorrelativo.ReadOnly = !valor;
            this.textBoxIgv.ReadOnly = !valor;
            dateTimeFecha.Enabled = valor;
            comboBoxComprobante.Enabled = valor;
            textBoxCantidad.ReadOnly = !valor;
            textBoxPrecioCompra.ReadOnly = !valor;
            textBoxPrecioVenta.ReadOnly = !valor;
            textBoxStockActual.ReadOnly = !valor;
            textBoxDescuento.ReadOnly = !valor;
            dateTimeFechaVencimiento.Enabled = valor;

            this.buttonBuscarArticulo.Enabled = valor;
            this.buttonBuscarCliente.Enabled = valor;

            this.buttonAgregar.Enabled = valor;
            this.buttonQuitar.Enabled = valor;
        }

        //Habilutar botones
        private void Botones()
        {
            if (this.IsNuevo)
            {
                this.Habilitar(true);
                this.buttonNuevo.Enabled = false;
                this.buttonGuardar.Enabled = true;
                this.buttonCancelar.Enabled = true;
            }
            else
            {
                this.Habilitar(false);
                this.buttonNuevo.Enabled = true;
                this.buttonGuardar.Enabled = false;
                this.buttonCancelar.Enabled = false;
            }
        }

        //Método Ocultar columnas
        private void OcultarColumnas()
        {
            this.dataListado.Columns[0].Visible = false;
            this.dataListado.Columns[1].Visible = false;
        }

        //Método Mostrar Registros
        private void Mostrar()
        {
            this.dataListado.DataSource = NVenta.Mostrar();
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        //Método BuscarFechas
        private void BuscarIngresoFecha()
        {
            this.dataListado.DataSource = NVenta.BuscarFechas(dateTimeFecha1.Value.ToString("dd/MM/yyyy"),
                dateTimeFecha2.Value.ToString("dd/MM/yyyy"));
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        private void MostrarDetalle()
        {
            this.dataListadoDetalle.DataSource = NVenta.MostrarDetalle(this.textBoxIdVenta.Text);
        }

        private void crearTabla()
        {
            this.dtDetalle = new DataTable("Detalle");
            this.dtDetalle.Columns.Add("iddetalle_ingreso", System.Type.GetType("System.Int32"));
            this.dtDetalle.Columns.Add("articulo", System.Type.GetType("System.String"));
            this.dtDetalle.Columns.Add("cantidad", System.Type.GetType("System.Int32"));
            this.dtDetalle.Columns.Add("precio_venta", System.Type.GetType("System.Decimal"));
            this.dtDetalle.Columns.Add("descuento", System.Type.GetType("System.Decimal"));
            this.dtDetalle.Columns.Add("subtotal", System.Type.GetType("System.Decimal"));

            //Relacionado el datagrid con el datatable
            this.dataListadoDetalle.DataSource = this.dtDetalle;
        }

        private void FrmVenta_Load(object sender, EventArgs e)
        {
            this.Mostrar();
            this.Habilitar(false);
            this.Botones();
            this.crearTabla();
            
        }

        private void FrmVenta_FormClosing(object sender, FormClosingEventArgs e)
        {
            _instancia = null;
        }

        private void buttonBuscarCliente_Click(object sender, EventArgs e)
        {
            FrmVistaCliente_Venta vista = new FrmVistaCliente_Venta();
            vista.ShowDialog();
        }

        private void buttonBuscarArticulo_Click(object sender, EventArgs e)
        {
            FrmVistaArticulo_Venta vista = new FrmVistaArticulo_Venta();
            vista.ShowDialog();
        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            this.BuscarIngresoFecha();
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente desea eliminar los registros?", "Sistema de Ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    string Codigo;
                    string Rpta = "";

                    foreach (DataGridViewRow row in dataListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToString(row.Cells[1].Value);
                            Rpta = NVenta.Eliminar(Convert.ToInt32(Codigo));
                        }
                    }
                    if (Rpta.Equals("OK"))
                    {
                        this.MensajeOk("Se Eliminó Correctamente la venta");
                    }
                    else
                    {
                        this.MensajeError(Rpta);
                    }
                    this.Mostrar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void dataListado_DoubleClick(object sender, EventArgs e)
        {
            this.textBoxIdVenta.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["idventa"].Value);
            this.textBoxCliente.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["cliente"].Value);
            this.dateTimeFecha.Value = Convert.ToDateTime(this.dataListado.CurrentRow.Cells["fecha"].Value);
            this.comboBoxComprobante.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["tipo_comprobante"].Value);
            this.textBoxSerie.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["serie"].Value);
            this.textBoxCorrelativo.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["correlativo"].Value);
            //this.labelTotalPagado.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["total"].Value);
            this.labelTotalPagado.Text =
                Convertir.numeroAletras(decimal.Parse(Convert.ToString(this.dataListado.CurrentRow.Cells["total"].Value)));
            //labelTotalPagado.Text = conversion.enletras(totalPagado.ToString()).ToLower();
            this.MostrarDetalle();
            this.tabControl1.SelectedIndex = 1;
        }

        private void checkBoxEliminar_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEliminar.Checked)
            {
                this.dataListado.Columns[0].Visible = true;
            }
            else
            {
                this.dataListado.Columns[0].Visible = false;
            }
        }

        private void dataListado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataListado.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell checkBoxEliminar =
                    (DataGridViewCheckBoxCell)dataListado.Rows[e.RowIndex].Cells["Eliminar"];
                checkBoxEliminar.Value = !Convert.ToBoolean(checkBoxEliminar.Value);
            }
        }

        private void buttonNuevo_Click(object sender, EventArgs e)
        {
            this.IsNuevo = true;
            this.Botones();
            this.Limpiar();
            this.limpiarDetalle();
            this.Habilitar(true);
            this.textBoxSerie.Focus();
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.IsNuevo = false;
            this.Botones();
            this.Limpiar();
            this.limpiarDetalle();
            this.Habilitar(false);
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string rpta = "";
                if (this.textBoxIdCliente.Text == string.Empty ||
                    this.textBoxSerie.Text == string.Empty ||
                    this.textBoxCorrelativo.Text == string.Empty ||
                    this.textBoxIgv.Text == string.Empty)
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcon.SetError(textBoxIdCliente, "Ingrese un valor");
                    errorIcon.SetError(textBoxSerie, "Ingrese un valor");
                    errorIcon.SetError(textBoxCorrelativo, "Ingrese un valor");
                    errorIcon.SetError(textBoxIgv, "Ingrese un valor");
                }
                else
                {
                    if (this.IsNuevo)
                    {
                        rpta = NVenta.Insertar(
                            Convert.ToInt32(textBoxIdCliente.Text),
                            idtrabajador,
                            dateTimeFecha.Value,
                            comboBoxComprobante.Text,
                            textBoxSerie.Text,
                            textBoxCorrelativo.Text,
                            Convert.ToDecimal(textBoxIgv.Text),
                            dtDetalle);
                    }

                    if (rpta.Equals("OK"))
                    {
                        if (this.IsNuevo)
                        {
                            this.MensajeOk("Se insertó de forma correcta el registro");
                        }

                    }
                    else
                    {
                        this.MensajeError(rpta);
                    }

                    this.IsNuevo = false;
                    this.Botones();
                    this.Limpiar();
                    this.limpiarDetalle();
                    this.Mostrar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void buttonAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.textBoxIdArticulo.Text == string.Empty ||
                    this.textBoxCantidad.Text == string.Empty ||
                    this.textBoxDescuento.Text == string.Empty ||
                    this.textBoxPrecioVenta.Text == string.Empty)
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcon.SetError(textBoxIdArticulo, "Ingrese un valor");
                    errorIcon.SetError(textBoxCantidad, "Ingrese un valor");
                    errorIcon.SetError(textBoxDescuento, "Ingrese un valor");
                    errorIcon.SetError(textBoxPrecioVenta, "Ingrese un valor");
                }
                else
                {
                    bool registrar = true;
                    foreach (DataRow row in dtDetalle.Rows)
                    {
                        if (Convert.ToInt32(row["iddetalle_ingreso"]) == Convert.ToInt32(this.textBoxIdArticulo.Text))
                        {
                            registrar = false;
                            this.MensajeError("EL ARTICULO SE ENCUENTRA AGREGADO EN EL DETALLE");
                        }
                    }
                    if (registrar && Convert.ToInt32(textBoxCantidad.Text) <= Convert.ToInt32(textBoxStockActual.Text))
                    {
                        decimal subTotal = Convert.ToDecimal(textBoxCantidad.Text) * Convert.ToDecimal(textBoxPrecioVenta.Text)-Convert.ToDecimal(this.textBoxDescuento.Text);
                        totalPagado = totalPagado + subTotal;
                        
                        labelTotalPagado.Text = Convertir.numeroAletras(decimal.Parse(totalPagado.ToString()));
                        
                        DataRow row = this.dtDetalle.NewRow();
                        row["iddetalle_ingreso"] = Convert.ToInt32(this.textBoxIdArticulo.Text);
                        row["articulo"] = this.textBoxArticulo.Text;
                        row["cantidad"] = Convert.ToInt32(this.textBoxCantidad.Text);
                        row["precio_venta"] = Convert.ToDecimal(this.textBoxPrecioVenta.Text);
                        row["descuento"] = Convert.ToDecimal(this.textBoxDescuento.Text);
                        row["subtotal"] = subTotal;
                        this.dtDetalle.Rows.Add(row);
                        this.limpiarDetalle();
                    }
                    else
                    {
                        MensajeError("No hay Stock suficiente");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void buttonQuitar_Click(object sender, EventArgs e)
        {
            try
            {
                int indiceFila = this.dataListadoDetalle.CurrentCell.RowIndex;
                DataRow row = this.dtDetalle.Rows[indiceFila];
                //Disminuir el total pagado
                this.totalPagado = this.totalPagado - Convert.ToDecimal(row["subtotal"].ToString());
                //labelTotalPagado.Text = Convertir.numeroAletras(decimal.Parse(totalPagado.ToString()));
                labelTotalPagado.Text = Convertir.numeroAletras(decimal.Parse(totalPagado.ToString()));
                //remover la fila
                this.dtDetalle.Rows.Remove(row);
            }
            catch (Exception ex)
            {
                MensajeError("No HAY FILA PARA REMOVER");
            }
        }

        private void buttonComprobante_Click(object sender, EventArgs e)
        {
            FrmReporteFactura frm = new FrmReporteFactura();
            frm.Idventa = Convert.ToInt32(this.dataListado.CurrentRow.Cells["idventa"].Value);
            frm.ShowDialog();
        }
    }
}
