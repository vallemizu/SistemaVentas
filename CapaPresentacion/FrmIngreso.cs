﻿using System;
using System.Data;
using System.Windows.Forms;

using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmIngreso : Form
    {
        public int idtrabajador;
        private bool IsNuevo;
        private DataTable dtDetalle;
        private decimal totalPagado = 0;

        private static FrmIngreso _instancia;

        public static FrmIngreso GetInstancia()
        {
            if (_instancia==null)
            {
                _instancia=new FrmIngreso();
            }
            return _instancia;
        }

        public void setProveedor(string idproveedor, string nombre)
        {
            this.textBoxIdProveedor.Text = idproveedor;
            this.textBoxProveedor.Text = nombre;
        }

        public void setArticulo(string idarticulo, string nombre)
        {
            textBoxIdArticulo.Text = idarticulo;
            textBoxArticulo.Text = nombre;
        }

        public FrmIngreso()
        {
            InitializeComponent();
            this.ttMensaje.SetToolTip(textBoxProveedor,"Seleccione el proveedor");
            this.ttMensaje.SetToolTip(textBoxSerie, "ingrese la serie del comprobante");
            this.ttMensaje.SetToolTip(textBoxCorrelativo, "ingrese el número de comprobante");
            this.ttMensaje.SetToolTip(textBoxStock, "Seleccione la cantidad de compra");
            this.ttMensaje.SetToolTip(textBoxArticulo, "Seleccione el artículo de compra");

            textBoxIdProveedor.Visible = false;
            textBoxIdArticulo.Visible = false;
            textBoxProveedor.ReadOnly = true;
            textBoxArticulo.ReadOnly = true;
        }

        //Mostrar Mensaje de confirmación
        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema de Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema de Ventas", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //Limpiar controles
        private void Limpiar()
        {
            this.textBoxIdIngreso.Text = string.Empty;
            this.textBoxIdProveedor.Text = string.Empty;
            this.textBoxProveedor.Text = string.Empty;
            this.textBoxSerie.Text = string.Empty;
            this.textBoxCorrelativo.Text = string.Empty;
            this.textBoxIgv.Text = string.Empty;
            this.labelTotalPagado.Text = "0,0";
            this.textBoxIgv.Text = "18";
            this.crearTabla();
        }

        private void limpiarDetalle()
        {
            textBoxIdArticulo.Text = string.Empty;
            textBoxArticulo.Text = string.Empty;
            textBoxStock.Text = string.Empty;
            textBoxPrecioCompra.Text=string.Empty;
            textBoxPrecioVenta.Text = string.Empty;
        }

        //Habilitar controles del formulario
        private void Habilitar(bool valor)
        {
            this.textBoxIdIngreso.ReadOnly = !valor;
            this.textBoxSerie.ReadOnly = !valor;
            this.textBoxCorrelativo.ReadOnly = !valor;
            this.textBoxIgv.ReadOnly = !valor;
            dateTimeFecha.Enabled = valor;
            comboBoxComprobante.Enabled = valor;
            textBoxStock.ReadOnly = !valor;
            textBoxPrecioCompra.ReadOnly = !valor;
            textBoxPrecioVenta.ReadOnly = !valor;
            dateTimeFechaProduccion.Enabled = valor;
            dateTimeFechaVencimiento.Enabled = valor;

            this.buttonBuscarArticulo.Enabled = valor;
            this.buttonBuscarProveedor.Enabled = valor;

            this.buttonAgregar.Enabled = valor;
            this.buttonQuitar.Enabled = valor;
        }

        //Habilutar botones
        private void Botones()
        {
            if (this.IsNuevo)
            {
                this.Habilitar(true);
                this.buttonNuevo.Enabled = false;
                this.buttonGuardar.Enabled = true;
                this.buttonCancelar.Enabled = true;
            }
            else
            {
                this.Habilitar(false);
                this.buttonNuevo.Enabled = true;
                this.buttonGuardar.Enabled = false;
                this.buttonCancelar.Enabled = false;
            }
        }

        //Método Ocultar columnas
        private void OcultarColumnas()
        {
            this.dataListado.Columns[0].Visible = false;
            this.dataListado.Columns[1].Visible = false;
        }

        //Método Mostrar Registros
        private void Mostrar()
        {
            this.dataListado.DataSource = NIngreso.Mostrar();
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        //Método BuscarFechas
        private void BuscarIngresoFecha()
        {
            this.dataListado.DataSource = NIngreso.BuscarIngresoFecha(dateTimeFecha1.Value.ToString("dd/MM/yyyy"), 
                dateTimeFecha2.Value.ToString("dd/MM/yyyy"));
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        private void MostrarDetalle()
        {
            this.dataListadoDetalle.DataSource = NIngreso.MostrarDetalle(this.textBoxIdIngreso.Text);
        }

        private void crearTabla()
        {
            this.dtDetalle = new DataTable("Detalle");
            this.dtDetalle.Columns.Add("idarticulo",System.Type.GetType("System.Int32"));
            this.dtDetalle.Columns.Add("articulo", System.Type.GetType("System.String"));
            this.dtDetalle.Columns.Add("precio_compra", System.Type.GetType("System.Decimal"));
            this.dtDetalle.Columns.Add("precio_venta", System.Type.GetType("System.Decimal"));
            this.dtDetalle.Columns.Add("stock_inicial", System.Type.GetType("System.Int32"));
            this.dtDetalle.Columns.Add("fecha_produccion", System.Type.GetType("System.DateTime"));
            this.dtDetalle.Columns.Add("fecha_vencimiento", System.Type.GetType("System.DateTime"));
            this.dtDetalle.Columns.Add("subtotal", System.Type.GetType("System.Decimal"));

            //Relacionado el datagrid con el datatable
            this.dataListadoDetalle.DataSource = this.dtDetalle;
        }

        private void FrmIngreso_Load(object sender, EventArgs e)
        {
            this.Mostrar();
            Habilitar(false);
            Botones();
            crearTabla();
        }

        private void FrmIngreso_FormClosing(object sender, FormClosingEventArgs e)
        {
            _instancia = null;
        }

        private void buttonBuscarProveedor_Click(object sender, EventArgs e)
        {
            FrmVistaProveedorIngreso vista= new FrmVistaProveedorIngreso();
            vista.ShowDialog();
        }

        private void buttonBuscarArticulo_Click(object sender, EventArgs e)
        {
            FrmVistaArticuloIngreso vista = new FrmVistaArticuloIngreso();
            vista.ShowDialog();
        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            this.BuscarIngresoFecha();
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente desea anular los registros?", "Sistema de Ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    string Codigo;
                    string Rpta = "";

                    foreach (DataGridViewRow row in dataListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToString(row.Cells[1].Value);
                            Rpta = NIngreso.Anular(Convert.ToInt32(Codigo));
                        }
                    }
                    if (Rpta.Equals("OK"))
                    {
                        this.MensajeOk("Se Anuló Correctamente el registro");
                    }
                    else
                    {
                        this.MensajeError(Rpta);
                    }
                    this.Mostrar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void checkBoxEliminar_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEliminar.Checked)
            {
                this.dataListado.Columns[0].Visible = true;
            }
            else
            {
                this.dataListado.Columns[0].Visible = false;
            }
        }

        private void dataListado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataListado.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell checkBoxEliminar =
                    (DataGridViewCheckBoxCell)dataListado.Rows[e.RowIndex].Cells["Eliminar"];
                checkBoxEliminar.Value = !Convert.ToBoolean(checkBoxEliminar.Value);
            }
        }

        private void buttonNuevo_Click(object sender, EventArgs e)
        {
            this.IsNuevo = true;
            this.Botones();
            this.Limpiar();
            this.Habilitar(true);
            this.textBoxSerie.Focus();
            this.limpiarDetalle();
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.IsNuevo = false;
            this.Botones();
            this.Limpiar();
            this.Habilitar(false);
            this.limpiarDetalle();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string rpta = "";
                if (this.textBoxIdProveedor.Text == string.Empty ||
                    this.textBoxSerie.Text == string.Empty ||
                    this.textBoxCorrelativo.Text == string.Empty ||
                    this.textBoxIgv.Text == string.Empty)
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcon.SetError(textBoxIdProveedor, "Ingrese un valor");
                    errorIcon.SetError(textBoxSerie, "Ingrese un valor");
                    errorIcon.SetError(textBoxCorrelativo, "Ingrese un valor");
                    errorIcon.SetError(textBoxIgv, "Ingrese un valor");
                }
                else
                {

                    if (this.IsNuevo)
                    {
                        rpta = NIngreso.Insertar(
                            this.idtrabajador,
                            Convert.ToInt32(textBoxIdProveedor.Text),
                            dateTimeFecha.Value,
                            comboBoxComprobante.Text,
                            textBoxSerie.Text,
                            textBoxCorrelativo.Text,
                            Convert.ToDecimal(textBoxIgv.Text),
                            "EMITIDO",
                            dtDetalle);
                    }
                    
                    if (rpta.Equals("OK"))
                    {
                        if (this.IsNuevo)
                        {
                            this.MensajeOk("Se insertó de forma correcta el registro");
                        }
                        
                    }
                    else
                    {
                        this.MensajeError(rpta);
                    }

                    this.IsNuevo = false;
                    this.Botones();
                    this.Limpiar();
                    this.limpiarDetalle();
                    this.Mostrar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void buttonAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.textBoxIdArticulo.Text == string.Empty ||
                    this.textBoxStock.Text == string.Empty ||
                    this.textBoxPrecioCompra.Text == string.Empty ||
                    this.textBoxPrecioVenta.Text == string.Empty)
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcon.SetError(textBoxIdArticulo, "Ingrese un valor");
                    errorIcon.SetError(textBoxStock, "Ingrese un valor");
                    errorIcon.SetError(textBoxPrecioCompra, "Ingrese un valor");
                    errorIcon.SetError(textBoxPrecioVenta, "Ingrese un valor");
                }
                else
                {
                    bool registrar = true;
                    foreach (DataRow row in dtDetalle.Rows)
                    {
                        if (Convert.ToInt32(row["idarticulo"])==Convert.ToInt32(textBoxIdArticulo.Text))
                        {
                            registrar = false;
                            this.MensajeError("EL ARTICULO SE ENCUENTRA AGREGADO EN EL DETALLE");
                        }
                    }
                    if (registrar)
                    {
                        decimal subTotal=Convert.ToDecimal(textBoxStock.Text)*Convert.ToDecimal(textBoxPrecioCompra.Text);
                        totalPagado = totalPagado + subTotal;
                        labelTotalPagado.Text = totalPagado.ToString("#0.00#");
                        //agregar ese detalle al datalistadoDetalle
                        DataRow row = this.dtDetalle.NewRow();
                        row["idarticulo"] = Convert.ToInt32(this.textBoxIdArticulo.Text);
                        row["articulo"] = this.textBoxArticulo.Text;
                        row["precio_compra"] = Convert.ToDecimal(this.textBoxPrecioCompra.Text);
                        row["precio_venta"] = Convert.ToDecimal(this.textBoxPrecioVenta.Text);
                        row["stock_inicial"] = Convert.ToInt32(this.textBoxStock.Text);
                        row["fecha_produccion"] = this.dateTimeFechaProduccion.Value;
                        row["fecha_vencimiento"] = this.dateTimeFechaVencimiento.Value;
                        row["subtotal"] = subTotal;
                        this.dtDetalle.Rows.Add(row);
                        this.limpiarDetalle();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void buttonQuitar_Click(object sender, EventArgs e)
        {
            try
            {
                int indiceFila = this.dataListadoDetalle.CurrentCell.RowIndex;
                DataRow row = this.dtDetalle.Rows[indiceFila];
                //Disminuir el total pagado
                this.totalPagado = this.totalPagado - Convert.ToDecimal(row["subtotal"].ToString());
                this.labelTotalPagado.Text = totalPagado.ToString("#0.00#");
                //remover la fila
                this.dtDetalle.Rows.Remove(row);
            }
            catch (Exception ex)
            {
                MensajeError("No HAY FILA PARA REMOVER");
            }
        }

        private void dataListado_DoubleClick(object sender, EventArgs e)
        {
            this.textBoxIdIngreso.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["idingreso"].Value);
            this.textBoxProveedor.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["proveedor"].Value);
            this.dateTimeFecha.Value = Convert.ToDateTime(this.dataListado.CurrentRow.Cells["fecha"].Value);
            this.comboBoxComprobante.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["tipo_comprobante"].Value);
            this.textBoxSerie.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["serie"].Value);
            this.textBoxCorrelativo.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["correlativo"].Value);
            this.labelTotalPagado.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["total"].Value);
            this.MostrarDetalle();
            this.tabControl1.SelectedIndex = 1;
        }
    }
}
