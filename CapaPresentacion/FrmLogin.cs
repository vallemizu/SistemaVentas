﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
            horaLabel.Text = DateTime.Now.ToString();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            horaLabel.Text = DateTime.Now.ToString();
        }

        private void salirButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ingresarButton_Click(object sender, EventArgs e)
        {
            DataTable Datos = CapaNegocio.NTrabajador.Login(this.usuaioTextBox.Text,
                this.passwordTextBox.Text);
            //Evaluar la existencia del usuario
            if (Datos.Rows.Count==0)
            {
                MessageBox.Show("No tiene acceso al sistema", "Sistema de Ventas", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                usuaioTextBox.Text = string.Empty;
                passwordTextBox.Text = string.Empty;
                usuaioTextBox.Focus();
            }
            else
            {
                FrmPrincipal frm = new FrmPrincipal();
                frm.Idtrabajador = Datos.Rows[0][0].ToString();
                frm.Apellidos = Datos.Rows[0][1].ToString();
                frm.Nombre = Datos.Rows[0][2].ToString();
                frm.Acceso = Datos.Rows[0][3].ToString();
                
                frm.Show();
                this.Hide();

            }
        }
    }
}
