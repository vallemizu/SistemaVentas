﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmArticulo : Form
    {
        private bool IsNuevo = false;

        private bool IsEditar = false;

        private static FrmArticulo _Instancia;

        public static FrmArticulo GetInstancia()
        {
            if (_Instancia==null)
            {
                _Instancia = new FrmArticulo();
            }
            return _Instancia;
        }

        public void setCategoria(string idcategoria, string nombre)
        {
            this.textBoxIdCategoria.Text = idcategoria;
            this.textBoxCategoria.Text = nombre;
        }

        public FrmArticulo()
        {
            InitializeComponent();
            this.ttMensaje.SetToolTip(this.textBoxNombre, "Ingrese el Nombre del Artículo");
            this.ttMensaje.SetToolTip(this.pictureBoxImage, "Seleccione la Imágen del Artículo");
            this.ttMensaje.SetToolTip(this.textBoxCategoria, "Seleccione la Categoría del Artículo");
            this.ttMensaje.SetToolTip(this.comboBoxPresentacion, "Seleccione la presentación del Artículo");

            textBoxIdCategoria.Visible = false;
            textBoxCategoria.ReadOnly = true;
            LlenarComboPresentacion();
        }

        //Mostrar Mensaje de confirmación
        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema de Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema de Ventas", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //Limpiar controles
        private void Limpiar()
        {
            this.textBoxCodigo.Text = string.Empty;
            this.textBoxNombre.Text = string.Empty;
            this.textBoxDescripcion.Text = string.Empty;
            this.textBoxIdCategoria.Text = string.Empty;
            this.textBoxCategoria.Text = string.Empty;
            this.textBoxIdArticulo.Text = string.Empty;
            this.pictureBoxImage.Image = global::CapaPresentacion.Properties.Resources.file;
        }

        //Habilitar controles del formulario
        private void Habilitar(bool valor)
        {
            this.textBoxCodigo.ReadOnly = !valor;
            this.textBoxNombre.ReadOnly = !valor;
            this.textBoxDescripcion.ReadOnly = !valor;
            this.buttonBuscarCategoria.Enabled = valor;
            this.comboBoxPresentacion.Enabled = valor;
            this.buttonCargar.Enabled = valor;
            this.buttonLimpiar.Enabled = valor;
            this.textBoxIdArticulo.ReadOnly = !valor;
        }

        //Habilutar botones
        private void Botones()
        {
            if (this.IsNuevo || this.IsEditar)
            {
                this.Habilitar(true);
                this.buttonNuevo.Enabled = false;
                this.buttonGuardar.Enabled = true;
                this.buttonEditar.Enabled = false;
                this.buttonCancelar.Enabled = true;
            }
            else
            {
                this.Habilitar(false);
                this.buttonNuevo.Enabled = true;
                this.buttonGuardar.Enabled = false;
                this.buttonEditar.Enabled = true;
                this.buttonCancelar.Enabled = false;
            }
        }

        //Método Ocultar columnas
        private void OcultarColumnas()
        {
            this.dataListado.Columns[0].Visible = false;
            this.dataListado.Columns[1].Visible = false;
            this.dataListado.Columns[6].Visible = false;
            this.dataListado.Columns[8].Visible = false;
        }

        //Método Mostrar Registros
        private void Mostrar()
        {
            this.dataListado.DataSource = NArticulo.Mostrar();
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        //Método BuscarNombre
        private void BuscarNombre()
        {
            this.dataListado.DataSource = NArticulo.BuscarNombre(this.textBoxBuscar.Text);
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        private void LlenarComboPresentacion()
        {
            comboBoxPresentacion.DataSource = NPresentacion.Mostrar();
            comboBoxPresentacion.ValueMember = "idpresentacion";
            comboBoxPresentacion.DisplayMember = "nombre";
        }

        private void buttonCargar_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result==DialogResult.OK)
            {
                this.pictureBoxImage.SizeMode=PictureBoxSizeMode.StretchImage;
                this.pictureBoxImage.Image=Image.FromFile(dialog.FileName);
            }
        }

        private void FrmArticulo_Load(object sender, EventArgs e)
        {
            this.Mostrar();
            this.Habilitar(false);
            this.Botones();
        }

        private void buttonLimpiar_Click(object sender, EventArgs e)
        {
            this.pictureBoxImage.SizeMode=PictureBoxSizeMode.StretchImage;
            this.pictureBoxImage.Image = global::CapaPresentacion.Properties.Resources.file;
        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            this.BuscarNombre();
        }

        private void textBoxBuscar_TextChanged(object sender, EventArgs e)
        {
            this.BuscarNombre();
        }

        private void buttonNuevo_Click(object sender, EventArgs e)
        {
            this.IsNuevo = true;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();
            this.Habilitar(true);
            this.textBoxNombre.Focus();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string rpta = "";
                if (this.textBoxNombre.Text == string.Empty || 
                    this.textBoxIdCategoria.Text==string.Empty ||
                    this.textBoxCodigo.Text == string.Empty)
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcon.SetError(textBoxNombre, "Ingrese un valor");
                    errorIcon.SetError(textBoxCodigo, "Ingrese un valor");
                    errorIcon.SetError(textBoxCategoria, "Ingrese un valor");
                }
                else
                {

                    System.IO.MemoryStream ms = new System.IO.MemoryStream();
                    this.pictureBoxImage.Image.Save(ms,System.Drawing.Imaging.ImageFormat.Png);

                    byte[] imagen = ms.GetBuffer();

                    if (this.IsNuevo)
                    {
                        rpta = NArticulo.Insertar(
                            this.textBoxCodigo.Text,
                            this.textBoxNombre.Text.Trim().ToUpper(),
                            this.textBoxDescripcion.Text.Trim(),
                            imagen,
                            Convert.ToInt32(this.textBoxIdCategoria.Text),
                            Convert.ToInt32(this.comboBoxPresentacion.SelectedValue)
                            );
                    }
                    else
                    {
                        rpta = NArticulo.Editar(
                            Convert.ToInt32(this.textBoxIdArticulo.Text),
                            this.textBoxCodigo.Text,
                            this.textBoxNombre.Text.Trim().ToUpper(),
                            this.textBoxDescripcion.Text.Trim(),
                            imagen, 
                            Convert.ToInt32(this.textBoxIdCategoria.Text),
                            Convert.ToInt32(this.comboBoxPresentacion.SelectedValue)
                            );
                    }

                    if (rpta.Equals("OK"))
                    {
                        if (this.IsNuevo)
                        {
                            this.MensajeOk("Se insertó de forma correcta el registro");
                        }
                        else
                        {
                            this.MensajeOk("Se actualizó de forma correcta el registro");
                        }
                    }
                    else
                    {
                        this.MensajeError(rpta);
                    }

                    this.IsNuevo = false;
                    this.IsEditar = false;
                    this.Botones();
                    this.Limpiar();
                    this.Mostrar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void buttonEditar_Click(object sender, EventArgs e)
        {
            if (!this.textBoxIdArticulo.Text.Equals(""))
            {
                this.IsEditar = true;
                this.Botones();
                this.Habilitar(true);
            }
            else
            {
                this.MensajeError("Debe seleccionar primero el registro a modificar");
            }
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.IsNuevo = false;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();
            this.Habilitar(false);
        }

        private void dataListado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataListado.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell checkBoxEliminar =
                    (DataGridViewCheckBoxCell)dataListado.Rows[e.RowIndex].Cells["Eliminar"];
                checkBoxEliminar.Value = !Convert.ToBoolean(checkBoxEliminar.Value);
            }
        }

        private void dataListado_DoubleClick(object sender, EventArgs e)
        {
            this.textBoxIdArticulo.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["idarticulo"].Value);
            this.textBoxCodigo.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["codigo"].Value);
            this.textBoxNombre.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["nombre"].Value);
            this.textBoxDescripcion.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["descripcion"].Value);

            byte[] imagenBuffer = (byte[])this.dataListado.CurrentRow.Cells["imagen"].Value;
            System.IO.MemoryStream ms = new System.IO.MemoryStream(imagenBuffer);

            this.pictureBoxImage.Image=Image.FromStream(ms);
            this.pictureBoxImage.SizeMode=PictureBoxSizeMode.StretchImage;

            this.textBoxIdCategoria.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["idcategoria"].Value);
            this.textBoxCategoria.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["Categoria"].Value);
            this.comboBoxPresentacion.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["Presentacion"].Value);

            this.tabControl1.SelectedIndex = 1;
        }

        private void checkBoxEliminar_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEliminar.Checked)
            {
                this.dataListado.Columns[0].Visible = true;
            }
            else
            {
                this.dataListado.Columns[0].Visible = false;
            }
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente desea elimar los registros?", "Sistema de Ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    string Codigo;
                    string Rpta = "";

                    foreach (DataGridViewRow row in dataListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToString(row.Cells[1].Value);
                            Rpta = NArticulo.Eliminar(Convert.ToInt32(Codigo));
                        }
                    }
                    if (Rpta.Equals("OK"))
                    {
                        this.MensajeOk("Se Eliminó Correctamente el registro");
                    }
                    else
                    {
                        this.MensajeError(Rpta);
                    }
                    this.Mostrar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void buttonBuscarCategoria_Click(object sender, EventArgs e)
        {
            FrmVistaCategoria_Articulo form = new FrmVistaCategoria_Articulo();
            form.ShowDialog();
        }

        private void FrmArticulo_FormClosing(object sender, FormClosingEventArgs e)
        {
            _Instancia = null;
        }

        private void buttonImprimir_Click(object sender, EventArgs e)
        {
            FrmReporteArticulos frm = new FrmReporteArticulos();
            frm.ShowDialog();
        }

    }
}
