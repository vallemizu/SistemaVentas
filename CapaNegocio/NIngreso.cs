﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using CapaDatos;

namespace CapaNegocio
{
    public class NIngreso
    {
        public static string Insertar(
            int idtrabajador,
            int idproveedor,
            DateTime fecha,
            string tipoComprobante,
            string serie,
            string correlativo,
            decimal igv,
            string estado,
            DataTable dtDetalles)
        {
            DIngreso Obj = new DIngreso();
            Obj.Idtrabajador = idtrabajador;
            Obj.Idproveedor = idproveedor;
            Obj.Fecha = fecha;
            Obj.TipoComprobante = tipoComprobante;
            Obj.Serie = serie;
            Obj.Correlativo = correlativo;
            Obj.Igv = igv;
            Obj.Estado = estado;
            List<DDetalle_Ingreso> detalles = new List<DDetalle_Ingreso>();
            
            foreach (DataRow row in dtDetalles.Rows)
            {
                DDetalle_Ingreso detalle = new DDetalle_Ingreso();
                detalle.Idarticulo = Convert.ToInt32(row["idarticulo"].ToString());
                detalle.PrecioCompra = Convert.ToDecimal(row["precio_compra"].ToString());
                detalle.PrecioVenta= Convert.ToDecimal(row["precio_venta"].ToString());
                detalle.StockInicial = Convert.ToInt32(row["stock_inicial"].ToString());
                detalle.StockActual = Convert.ToInt32(row["stock_inicial"].ToString());
                detalle.FechaProduccion = Convert.ToDateTime(row["fecha_produccion"].ToString());
                detalle.FechaVencimiento = Convert.ToDateTime(row["fecha_vencimiento"].ToString());
                detalles.Add(detalle);
            }

            return Obj.Insertar(Obj, detalles);
        }

        public static string Anular(int idingreso)
        {
            DIngreso Obj = new DIngreso();
            Obj.Idingreso = idingreso;
            return Obj.Anular(Obj);
        }

        public static DataTable Mostrar()
        {
            return new DIngreso().Mostrar();
        }

        public static DataTable BuscarIngresoFecha(string textobuscar, string textobuscar2)
        {
            DIngreso Obj = new DIngreso();
            return Obj.BuscarIngresoFecha(textobuscar, textobuscar2);
        }

        public static DataTable MostrarDetalle(string textobuscar)
        {
            DIngreso Obj = new DIngreso();
            return Obj.MostrarDetalle(textobuscar);
        }
    }
}
