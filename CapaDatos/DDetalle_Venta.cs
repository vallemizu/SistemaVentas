﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace CapaDatos
{
    public class DDetalle_Venta
    {
        //iddetalle_venta, idventa, iddetalle_ingreso, cantidad, precio_venta, descuento
        private int _IddetalleVenta;
        private int _Idventa;
        private int _IddetalleIngreso;
        private int _Cantidad;
        private decimal _PrecioVenta;
        private decimal _Descuento;

        public int IddetalleVenta
        {
            get { return _IddetalleVenta; }
            set { _IddetalleVenta = value; }
        }

        public int Idventa
        {
            get { return _Idventa; }
            set { _Idventa = value; }
        }

        public int IddetalleIngreso
        {
            get { return _IddetalleIngreso; }
            set { _IddetalleIngreso = value; }
        }

        public int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }

        public decimal PrecioVenta
        {
            get { return _PrecioVenta; }
            set { _PrecioVenta = value; }
        }

        public decimal Descuento
        {
            get { return _Descuento; }
            set { _Descuento = value; }
        }

        public DDetalle_Venta()
        {
            
        }

        public DDetalle_Venta(int iddetalleVenta, int idventa, int iddetalleIngreso, 
            int cantidad, decimal precioVenta, decimal descuento)
        {
            IddetalleVenta = iddetalleVenta;
            Idventa = idventa;
            IddetalleIngreso = iddetalleIngreso;
            Cantidad = cantidad;
            PrecioVenta = precioVenta;
            Descuento = descuento;
        }

        public string Insertar(DDetalle_Venta DetalleVenta,
            ref SqlConnection SqlCon, ref SqlTransaction SqlTra)
        {
            string rpta = "";
            try
            {
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.Transaction = SqlTra;
                SqlCmd.CommandText = "spinsertar_detalle_venta";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParIddetalleVenta = new SqlParameter();
                ParIddetalleVenta.ParameterName = "@iddetalle_venta";
                ParIddetalleVenta.SqlDbType = SqlDbType.Int;
                ParIddetalleVenta.Direction = ParameterDirection.Output;
                SqlCmd.Parameters.Add(ParIddetalleVenta);

                SqlParameter ParIdventa = new SqlParameter();
                ParIdventa.ParameterName = "@idventa";
                ParIdventa.SqlDbType = SqlDbType.Int;
                ParIdventa.Value = DetalleVenta.Idventa;
                SqlCmd.Parameters.Add(ParIdventa);

                SqlParameter ParIddetalleIngreso = new SqlParameter();
                ParIddetalleIngreso.ParameterName = "@iddetalle_ingreso";
                ParIddetalleIngreso.SqlDbType = SqlDbType.Int;
                ParIddetalleIngreso.Value = DetalleVenta.IddetalleIngreso;
                SqlCmd.Parameters.Add(ParIddetalleIngreso);

                SqlParameter ParCantidad = new SqlParameter();
                ParCantidad.ParameterName = "@cantidad";
                ParCantidad.SqlDbType = SqlDbType.Int;
                ParCantidad.Value = DetalleVenta.Cantidad;
                SqlCmd.Parameters.Add(ParCantidad);

                SqlParameter ParPrecioVenta = new SqlParameter();
                ParPrecioVenta.ParameterName = "@precio_venta";
                ParPrecioVenta.SqlDbType = SqlDbType.Money;
                ParPrecioVenta.Value = DetalleVenta.PrecioVenta;
                SqlCmd.Parameters.Add(ParPrecioVenta);

                SqlParameter ParDescuento = new SqlParameter();
                ParDescuento.ParameterName = "@descuento";
                ParDescuento.SqlDbType = SqlDbType.Money;
                ParDescuento.Value = DetalleVenta.Descuento;
                SqlCmd.Parameters.Add(ParDescuento);

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO SE INGRESO EL REGISTRO";

            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }

            return rpta;
        }
    }
}
