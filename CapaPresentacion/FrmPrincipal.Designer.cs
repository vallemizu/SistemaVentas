﻿namespace CapaPresentacion
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.articulosTSB = new System.Windows.Forms.ToolStripButton();
            this.categoriasTSB = new System.Windows.Forms.ToolStripButton();
            this.presentacionTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ingresosTSB = new System.Windows.Forms.ToolStripButton();
            this.proveedoresTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ventasTSB = new System.Windows.Forms.ToolStripButton();
            this.clientesTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.trabajadorTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.nombreLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.apellidosLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.accesoLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.sisventasTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.salirTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.almacenTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.articulosTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriasTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.presentacionTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresosTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.proveedoresTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.ventaTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.ventasTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.mantenimientoTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.trabajadoresTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.consultasTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.ventasPorFechaTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasPorFechaTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.stockDeArticulosTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.verTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.herramientasTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.BaseDatosTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.backUpTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.ventanasTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.newWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cascadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrangeIconsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpTSM = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.BackColor = System.Drawing.Color.White;
            this.toolStrip.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.articulosTSB,
            this.categoriasTSB,
            this.presentacionTSB,
            this.toolStripSeparator1,
            this.ingresosTSB,
            this.proveedoresTSB,
            this.toolStripSeparator2,
            this.ventasTSB,
            this.clientesTSB,
            this.toolStripSeparator3,
            this.trabajadorTSB,
            this.toolStripSeparator4,
            this.toolStripButton1});
            this.toolStrip.Location = new System.Drawing.Point(0, 25);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(927, 52);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "ToolStrip";
            // 
            // articulosTSB
            // 
            this.articulosTSB.Image = global::CapaPresentacion.Properties.Resources.articulos;
            this.articulosTSB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.articulosTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.articulosTSB.Name = "articulosTSB";
            this.articulosTSB.Size = new System.Drawing.Size(60, 49);
            this.articulosTSB.Text = "Artículos";
            this.articulosTSB.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.articulosTSB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.articulosTSB.Click += new System.EventHandler(this.articulosTSB_Click);
            // 
            // categoriasTSB
            // 
            this.categoriasTSB.Image = global::CapaPresentacion.Properties.Resources.categorias;
            this.categoriasTSB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.categoriasTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.categoriasTSB.Name = "categoriasTSB";
            this.categoriasTSB.Size = new System.Drawing.Size(69, 49);
            this.categoriasTSB.Text = "Categorías";
            this.categoriasTSB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.categoriasTSB.Click += new System.EventHandler(this.categoriasTSB_Click);
            // 
            // presentacionTSB
            // 
            this.presentacionTSB.Image = global::CapaPresentacion.Properties.Resources.presentacion;
            this.presentacionTSB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.presentacionTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.presentacionTSB.Name = "presentacionTSB";
            this.presentacionTSB.Size = new System.Drawing.Size(83, 49);
            this.presentacionTSB.Text = "Presentación";
            this.presentacionTSB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.presentacionTSB.Click += new System.EventHandler(this.presentacionTSB_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 52);
            // 
            // ingresosTSB
            // 
            this.ingresosTSB.Image = global::CapaPresentacion.Properties.Resources.ingresos;
            this.ingresosTSB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ingresosTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ingresosTSB.Name = "ingresosTSB";
            this.ingresosTSB.Size = new System.Drawing.Size(58, 49);
            this.ingresosTSB.Text = "Ingresos";
            this.ingresosTSB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ingresosTSB.Click += new System.EventHandler(this.ingresosTSB_Click);
            // 
            // proveedoresTSB
            // 
            this.proveedoresTSB.Image = global::CapaPresentacion.Properties.Resources.proveedores;
            this.proveedoresTSB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.proveedoresTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.proveedoresTSB.Name = "proveedoresTSB";
            this.proveedoresTSB.Size = new System.Drawing.Size(82, 49);
            this.proveedoresTSB.Text = "Proveedores";
            this.proveedoresTSB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.proveedoresTSB.Click += new System.EventHandler(this.proveedoresTSB_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 52);
            // 
            // ventasTSB
            // 
            this.ventasTSB.Image = global::CapaPresentacion.Properties.Resources.ventas;
            this.ventasTSB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ventasTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ventasTSB.Name = "ventasTSB";
            this.ventasTSB.Size = new System.Drawing.Size(48, 49);
            this.ventasTSB.Text = "Ventas";
            this.ventasTSB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ventasTSB.Click += new System.EventHandler(this.ventasTSB_Click);
            // 
            // clientesTSB
            // 
            this.clientesTSB.Image = global::CapaPresentacion.Properties.Resources.clientes;
            this.clientesTSB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.clientesTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clientesTSB.Name = "clientesTSB";
            this.clientesTSB.Size = new System.Drawing.Size(55, 49);
            this.clientesTSB.Text = "Clientes";
            this.clientesTSB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.clientesTSB.Click += new System.EventHandler(this.clientesTSB_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 52);
            // 
            // trabajadorTSB
            // 
            this.trabajadorTSB.Image = global::CapaPresentacion.Properties.Resources.trabajador;
            this.trabajadorTSB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.trabajadorTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.trabajadorTSB.Name = "trabajadorTSB";
            this.trabajadorTSB.Size = new System.Drawing.Size(69, 49);
            this.trabajadorTSB.Text = "Trabajador";
            this.trabajadorTSB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.trabajadorTSB.Click += new System.EventHandler(this.trabajadorTSB_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 52);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::CapaPresentacion.Properties.Resources.PowerOff;
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(35, 49);
            this.toolStripButton1.Text = "Salir";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.Color.White;
            this.statusStrip.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.tssLabel,
            this.nombreLabel,
            this.apellidosLabel,
            this.toolStripStatusLabel2,
            this.accesoLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 408);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(927, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(108, 17);
            this.toolStripStatusLabel.Text = "Sistema de Ventas";
            // 
            // tssLabel
            // 
            this.tssLabel.Name = "tssLabel";
            this.tssLabel.Size = new System.Drawing.Size(61, 17);
            this.tssLabel.Text = "Nombres:";
            // 
            // nombreLabel
            // 
            this.nombreLabel.ForeColor = System.Drawing.Color.DarkViolet;
            this.nombreLabel.Name = "nombreLabel";
            this.nombreLabel.Size = new System.Drawing.Size(51, 17);
            this.nombreLabel.Text = "nombre";
            // 
            // apellidosLabel
            // 
            this.apellidosLabel.ForeColor = System.Drawing.Color.DarkViolet;
            this.apellidosLabel.Name = "apellidosLabel";
            this.apellidosLabel.Size = new System.Drawing.Size(55, 17);
            this.apellidosLabel.Text = "apellidos";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabel2.Text = "Acceso:";
            // 
            // accesoLabel
            // 
            this.accesoLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.accesoLabel.Name = "accesoLabel";
            this.accesoLabel.Size = new System.Drawing.Size(44, 17);
            this.accesoLabel.Text = "acceso";
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.LightBlue;
            this.menuStrip.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sisventasTSM,
            this.almacenTSM,
            this.comprasTSM,
            this.ventaTSM,
            this.mantenimientoTSM,
            this.consultasTSM,
            this.verTSM,
            this.herramientasTSM,
            this.ventanasTSM,
            this.helpTSM});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.MdiWindowListItem = this.ventanasTSM;
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(927, 25);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // sisventasTSM
            // 
            this.sisventasTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirTSM});
            this.sisventasTSM.Image = global::CapaPresentacion.Properties.Resources.Mac_Dashboard;
            this.sisventasTSM.Name = "sisventasTSM";
            this.sisventasTSM.Size = new System.Drawing.Size(92, 21);
            this.sisventasTSM.Text = "Sisventas";
            // 
            // salirTSM
            // 
            this.salirTSM.Image = global::CapaPresentacion.Properties.Resources.PowerOff;
            this.salirTSM.Name = "salirTSM";
            this.salirTSM.Size = new System.Drawing.Size(101, 22);
            this.salirTSM.Text = "&Salir";
            this.salirTSM.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // almacenTSM
            // 
            this.almacenTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.articulosTSM,
            this.categoriasTSM,
            this.presentacionTSM});
            this.almacenTSM.Image = global::CapaPresentacion.Properties.Resources.Dropbox;
            this.almacenTSM.Name = "almacenTSM";
            this.almacenTSM.Size = new System.Drawing.Size(88, 21);
            this.almacenTSM.Text = "Almacén";
            // 
            // articulosTSM
            // 
            this.articulosTSM.Image = global::CapaPresentacion.Properties.Resources.Windows_8_Store;
            this.articulosTSM.Name = "articulosTSM";
            this.articulosTSM.Size = new System.Drawing.Size(154, 22);
            this.articulosTSM.Text = "&Artículos";
            this.articulosTSM.Click += new System.EventHandler(this.articulosToolStripMenuItem_Click);
            // 
            // categoriasTSM
            // 
            this.categoriasTSM.Image = global::CapaPresentacion.Properties.Resources.Notepad;
            this.categoriasTSM.Name = "categoriasTSM";
            this.categoriasTSM.Size = new System.Drawing.Size(154, 22);
            this.categoriasTSM.Text = "&Categorías";
            this.categoriasTSM.Click += new System.EventHandler(this.categoriasToolStripMenuItem_Click);
            // 
            // presentacionTSM
            // 
            this.presentacionTSM.Image = global::CapaPresentacion.Properties.Resources.Aero_WinFlip_3D;
            this.presentacionTSM.Name = "presentacionTSM";
            this.presentacionTSM.Size = new System.Drawing.Size(154, 22);
            this.presentacionTSM.Text = "&Presentación";
            this.presentacionTSM.Click += new System.EventHandler(this.presentacionToolStripMenuItem_Click);
            // 
            // comprasTSM
            // 
            this.comprasTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingresosTSM,
            this.proveedoresTSM});
            this.comprasTSM.Image = global::CapaPresentacion.Properties.Resources.Windows_8_Store;
            this.comprasTSM.Name = "comprasTSM";
            this.comprasTSM.Size = new System.Drawing.Size(90, 21);
            this.comprasTSM.Text = "Compras";
            // 
            // ingresosTSM
            // 
            this.ingresosTSM.Image = global::CapaPresentacion.Properties.Resources.Live_Sync;
            this.ingresosTSM.Name = "ingresosTSM";
            this.ingresosTSM.Size = new System.Drawing.Size(152, 22);
            this.ingresosTSM.Text = "&Ingresos";
            this.ingresosTSM.Click += new System.EventHandler(this.ingresosTSM_Click);
            // 
            // proveedoresTSM
            // 
            this.proveedoresTSM.Image = global::CapaPresentacion.Properties.Resources.Live_Messenger;
            this.proveedoresTSM.Name = "proveedoresTSM";
            this.proveedoresTSM.Size = new System.Drawing.Size(152, 22);
            this.proveedoresTSM.Text = "&Proveedores";
            this.proveedoresTSM.Click += new System.EventHandler(this.proveedoresToolStripMenuItem_Click);
            // 
            // ventaTSM
            // 
            this.ventaTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ventasTSM,
            this.clientesTSM});
            this.ventaTSM.Image = global::CapaPresentacion.Properties.Resources.sales;
            this.ventaTSM.Name = "ventaTSM";
            this.ventaTSM.Size = new System.Drawing.Size(76, 21);
            this.ventaTSM.Text = "Ventas";
            // 
            // ventasTSM
            // 
            this.ventasTSM.Image = global::CapaPresentacion.Properties.Resources.cart;
            this.ventasTSM.Name = "ventasTSM";
            this.ventasTSM.Size = new System.Drawing.Size(123, 22);
            this.ventasTSM.Text = "&Ventas";
            this.ventasTSM.Click += new System.EventHandler(this.ventasTSM_Click);
            // 
            // clientesTSM
            // 
            this.clientesTSM.Image = global::CapaPresentacion.Properties.Resources.Live_Messenger_alt_3;
            this.clientesTSM.Name = "clientesTSM";
            this.clientesTSM.Size = new System.Drawing.Size(123, 22);
            this.clientesTSM.Text = "&Clientes";
            this.clientesTSM.Click += new System.EventHandler(this.clientesToolStripMenuItem_Click);
            // 
            // mantenimientoTSM
            // 
            this.mantenimientoTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trabajadoresTSM});
            this.mantenimientoTSM.Image = global::CapaPresentacion.Properties.Resources.Configure_alt_3;
            this.mantenimientoTSM.Name = "mantenimientoTSM";
            this.mantenimientoTSM.Size = new System.Drawing.Size(129, 21);
            this.mantenimientoTSM.Text = "Mantenimiento";
            // 
            // trabajadoresTSM
            // 
            this.trabajadoresTSM.Image = global::CapaPresentacion.Properties.Resources.Personal;
            this.trabajadoresTSM.Name = "trabajadoresTSM";
            this.trabajadoresTSM.Size = new System.Drawing.Size(153, 22);
            this.trabajadoresTSM.Text = "&Trabajadores";
            this.trabajadoresTSM.Click += new System.EventHandler(this.trabajadoresToolStripMenuItem_Click);
            // 
            // consultasTSM
            // 
            this.consultasTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ventasPorFechaTSM,
            this.comprasPorFechaTSM,
            this.stockDeArticulosTSM});
            this.consultasTSM.Image = global::CapaPresentacion.Properties.Resources.Searches_Folder;
            this.consultasTSM.Name = "consultasTSM";
            this.consultasTSM.Size = new System.Drawing.Size(95, 21);
            this.consultasTSM.Text = "Consultas";
            // 
            // ventasPorFechaTSM
            // 
            this.ventasPorFechaTSM.Image = global::CapaPresentacion.Properties.Resources.cart;
            this.ventasPorFechaTSM.Name = "ventasPorFechaTSM";
            this.ventasPorFechaTSM.Size = new System.Drawing.Size(191, 22);
            this.ventasPorFechaTSM.Text = "&Ventas por fecha";
            // 
            // comprasPorFechaTSM
            // 
            this.comprasPorFechaTSM.Image = global::CapaPresentacion.Properties.Resources.Bookmarks;
            this.comprasPorFechaTSM.Name = "comprasPorFechaTSM";
            this.comprasPorFechaTSM.Size = new System.Drawing.Size(191, 22);
            this.comprasPorFechaTSM.Text = "&Compras por fecha";
            // 
            // stockDeArticulosTSM
            // 
            this.stockDeArticulosTSM.Image = global::CapaPresentacion.Properties.Resources.Downloads;
            this.stockDeArticulosTSM.Name = "stockDeArticulosTSM";
            this.stockDeArticulosTSM.Size = new System.Drawing.Size(191, 22);
            this.stockDeArticulosTSM.Text = "&Stock de Artículos";
            this.stockDeArticulosTSM.Click += new System.EventHandler(this.stockDeArticulosTSM_Click);
            // 
            // verTSM
            // 
            this.verTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBarToolStripMenuItem,
            this.statusBarToolStripMenuItem});
            this.verTSM.Image = global::CapaPresentacion.Properties.Resources.Zoom_In;
            this.verTSM.Name = "verTSM";
            this.verTSM.Size = new System.Drawing.Size(55, 21);
            this.verTSM.Text = "&Ver";
            // 
            // toolBarToolStripMenuItem
            // 
            this.toolBarToolStripMenuItem.Checked = true;
            this.toolBarToolStripMenuItem.CheckOnClick = true;
            this.toolBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolBarToolStripMenuItem.Name = "toolBarToolStripMenuItem";
            this.toolBarToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.toolBarToolStripMenuItem.Text = "&Barra de herramientas";
            this.toolBarToolStripMenuItem.Click += new System.EventHandler(this.ToolBarToolStripMenuItem_Click);
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Checked = true;
            this.statusBarToolStripMenuItem.CheckOnClick = true;
            this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.statusBarToolStripMenuItem.Text = "&Barra de estado";
            this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.StatusBarToolStripMenuItem_Click);
            // 
            // herramientasTSM
            // 
            this.herramientasTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BaseDatosTSM});
            this.herramientasTSM.Image = global::CapaPresentacion.Properties.Resources.Configure_alt_1;
            this.herramientasTSM.Name = "herramientasTSM";
            this.herramientasTSM.Size = new System.Drawing.Size(118, 21);
            this.herramientasTSM.Text = "&Herramientas";
            // 
            // BaseDatosTSM
            // 
            this.BaseDatosTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backUpTSM});
            this.BaseDatosTSM.Image = global::CapaPresentacion.Properties.Resources.basedatos;
            this.BaseDatosTSM.Name = "BaseDatosTSM";
            this.BaseDatosTSM.Size = new System.Drawing.Size(162, 22);
            this.BaseDatosTSM.Text = "&Base de Datos";
            // 
            // backUpTSM
            // 
            this.backUpTSM.Image = global::CapaPresentacion.Properties.Resources.Backup_and_Restore;
            this.backUpTSM.Name = "backUpTSM";
            this.backUpTSM.Size = new System.Drawing.Size(125, 22);
            this.backUpTSM.Text = "Back Up";
            // 
            // ventanasTSM
            // 
            this.ventanasTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newWindowToolStripMenuItem,
            this.cascadeToolStripMenuItem,
            this.tileVerticalToolStripMenuItem,
            this.tileHorizontalToolStripMenuItem,
            this.closeAllToolStripMenuItem,
            this.arrangeIconsToolStripMenuItem});
            this.ventanasTSM.Image = global::CapaPresentacion.Properties.Resources.Troubleshooting;
            this.ventanasTSM.Name = "ventanasTSM";
            this.ventanasTSM.Size = new System.Drawing.Size(91, 21);
            this.ventanasTSM.Text = "&Ventanas";
            // 
            // newWindowToolStripMenuItem
            // 
            this.newWindowToolStripMenuItem.Name = "newWindowToolStripMenuItem";
            this.newWindowToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.newWindowToolStripMenuItem.Text = "&Nueva ventana";
            this.newWindowToolStripMenuItem.Click += new System.EventHandler(this.ShowNewForm);
            // 
            // cascadeToolStripMenuItem
            // 
            this.cascadeToolStripMenuItem.Name = "cascadeToolStripMenuItem";
            this.cascadeToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.cascadeToolStripMenuItem.Text = "&Cascada";
            this.cascadeToolStripMenuItem.Click += new System.EventHandler(this.CascadeToolStripMenuItem_Click);
            // 
            // tileVerticalToolStripMenuItem
            // 
            this.tileVerticalToolStripMenuItem.Name = "tileVerticalToolStripMenuItem";
            this.tileVerticalToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.tileVerticalToolStripMenuItem.Text = "Mosaico &vertical";
            this.tileVerticalToolStripMenuItem.Click += new System.EventHandler(this.TileVerticalToolStripMenuItem_Click);
            // 
            // tileHorizontalToolStripMenuItem
            // 
            this.tileHorizontalToolStripMenuItem.Name = "tileHorizontalToolStripMenuItem";
            this.tileHorizontalToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.tileHorizontalToolStripMenuItem.Text = "Mosaico &horizontal";
            this.tileHorizontalToolStripMenuItem.Click += new System.EventHandler(this.TileHorizontalToolStripMenuItem_Click);
            // 
            // closeAllToolStripMenuItem
            // 
            this.closeAllToolStripMenuItem.Name = "closeAllToolStripMenuItem";
            this.closeAllToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.closeAllToolStripMenuItem.Text = "C&errar todo";
            this.closeAllToolStripMenuItem.Click += new System.EventHandler(this.CloseAllToolStripMenuItem_Click);
            // 
            // arrangeIconsToolStripMenuItem
            // 
            this.arrangeIconsToolStripMenuItem.Name = "arrangeIconsToolStripMenuItem";
            this.arrangeIconsToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.arrangeIconsToolStripMenuItem.Text = "&Organizar iconos";
            this.arrangeIconsToolStripMenuItem.Click += new System.EventHandler(this.ArrangeIconsToolStripMenuItem_Click);
            // 
            // helpTSM
            // 
            this.helpTSM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indexToolStripMenuItem,
            this.toolStripSeparator8,
            this.aboutToolStripMenuItem});
            this.helpTSM.Image = global::CapaPresentacion.Properties.Resources.Default;
            this.helpTSM.Name = "helpTSM";
            this.helpTSM.Size = new System.Drawing.Size(75, 21);
            this.helpTSM.Text = "Ay&uda";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("indexToolStripMenuItem.Image")));
            this.indexToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.indexToolStripMenuItem.Text = "&Índice";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(154, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.aboutToolStripMenuItem.Text = "&Acerca de... ...";
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 430);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SISTEMA DE VENTAS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem verTSM;
        private System.Windows.Forms.ToolStripMenuItem toolBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem herramientasTSM;
        private System.Windows.Forms.ToolStripMenuItem BaseDatosTSM;
        private System.Windows.Forms.ToolStripMenuItem backUpTSM;
        private System.Windows.Forms.ToolStripMenuItem ventanasTSM;
        private System.Windows.Forms.ToolStripMenuItem newWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cascadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tileVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tileHorizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrangeIconsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpTSM;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem sisventasTSM;
        private System.Windows.Forms.ToolStripMenuItem salirTSM;
        private System.Windows.Forms.ToolStripMenuItem almacenTSM;
        private System.Windows.Forms.ToolStripMenuItem articulosTSM;
        private System.Windows.Forms.ToolStripMenuItem categoriasTSM;
        private System.Windows.Forms.ToolStripMenuItem presentacionTSM;
        private System.Windows.Forms.ToolStripMenuItem comprasTSM;
        private System.Windows.Forms.ToolStripMenuItem ingresosTSM;
        private System.Windows.Forms.ToolStripMenuItem proveedoresTSM;
        private System.Windows.Forms.ToolStripMenuItem ventaTSM;
        private System.Windows.Forms.ToolStripMenuItem ventasTSM;
        private System.Windows.Forms.ToolStripMenuItem clientesTSM;
        private System.Windows.Forms.ToolStripMenuItem mantenimientoTSM;
        private System.Windows.Forms.ToolStripMenuItem trabajadoresTSM;
        private System.Windows.Forms.ToolStripMenuItem consultasTSM;
        private System.Windows.Forms.ToolStripMenuItem ventasPorFechaTSM;
        private System.Windows.Forms.ToolStripMenuItem comprasPorFechaTSM;
        private System.Windows.Forms.ToolStripMenuItem stockDeArticulosTSM;
        private System.Windows.Forms.ToolStripButton articulosTSB;
        private System.Windows.Forms.ToolStripButton categoriasTSB;
        private System.Windows.Forms.ToolStripButton presentacionTSB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ingresosTSB;
        private System.Windows.Forms.ToolStripButton proveedoresTSB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton ventasTSB;
        private System.Windows.Forms.ToolStripButton clientesTSB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton trabajadorTSB;
        private System.Windows.Forms.ToolStripStatusLabel tssLabel;
        private System.Windows.Forms.ToolStripStatusLabel nombreLabel;
        private System.Windows.Forms.ToolStripStatusLabel apellidosLabel;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel accesoLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}



