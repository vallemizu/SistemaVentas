﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmTrabajador : Form
    {
        private bool IsNuevo = false;

        private bool IsEditar = false;

        public FrmTrabajador()
        {
            InitializeComponent();
            this.ttMensaje.SetToolTip(this.textBoxNombre, "Ingrese el Nombre");
            this.ttMensaje.SetToolTip(this.textBoxApellidos, "Ingrese los Apellidos");
            this.ttMensaje.SetToolTip(this.textBoxNumDocumento, "Ingrese el número de documento");
            this.ttMensaje.SetToolTip(this.textBoxDireccion, "Ingrese la Dirección");
            this.ttMensaje.SetToolTip(this.textBoxUsuario, "Ingrese el Usuario");
            this.ttMensaje.SetToolTip(this.textBoxContrasena, "Ingrese la Contraseña");
            this.ttMensaje.SetToolTip(this.comboBoxAcceso, "Ingrese el Acceso del Usuario");
        }

        //Mostrar Mensaje de confirmación
        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema de Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema de Ventas", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //Limpiar controles
        private void Limpiar()
        {
            this.textBoxNombre.Text = string.Empty;
            this.textBoxApellidos.Text = string.Empty;
            this.textBoxNumDocumento.Text = string.Empty;
            this.textBoxDireccion.Text = string.Empty;
            this.textBoxTelefono.Text = string.Empty;
            this.textBoxUsuario.Text = string.Empty;
            this.textBoxContrasena.Text = string.Empty;
            this.textBoxTelefono.Text = string.Empty;
            this.textBoxIdTrabajador.Text = string.Empty;
        }

        //Habilitar controles del formulario
        private void Habilitar(bool valor)
        {
            this.textBoxNombre.ReadOnly = !valor;
            this.textBoxApellidos.ReadOnly = !valor;
            this.textBoxNumDocumento.ReadOnly = !valor;
            this.textBoxDireccion.ReadOnly = !valor;
            this.textBoxTelefono.ReadOnly = !valor;
            this.comboBoxSexo.Enabled = valor;
            this.dateTimePickerFechaNacimiento.Enabled = valor;
            this.comboBoxAcceso.Enabled = valor;
            this.textBoxUsuario.ReadOnly = !valor;
            this.textBoxContrasena.ReadOnly = !valor;

            this.textBoxIdTrabajador.ReadOnly = !valor;

        }

        //Habilutar botones
        private void Botones()
        {
            if (this.IsNuevo || this.IsEditar)
            {
                this.Habilitar(true);
                this.buttonNuevo.Enabled = false;
                this.buttonGuardar.Enabled = true;
                this.buttonEditar.Enabled = false;
                this.buttonCancelar.Enabled = true;
            }
            else
            {
                this.Habilitar(false);
                this.buttonNuevo.Enabled = true;
                this.buttonGuardar.Enabled = false;
                this.buttonEditar.Enabled = true;
                this.buttonCancelar.Enabled = false;
            }
        }

        //Método Ocultar columnas
        private void OcultarColumnas()
        {
            this.dataListado.Columns[0].Visible = false;
            this.dataListado.Columns[1].Visible = false;
        }

        //Método Mostrar Registros
        private void Mostrar()
        {
            this.dataListado.DataSource = NTrabajador.Mostrar();
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        //Método BuscarNombre
        private void BuscarTrabajadorApellidos()
        {
            this.dataListado.DataSource = NTrabajador.BuscarTrabajadorApellidos(this.textBoxBuscar.Text);
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        //Método BuscarDoc
        private void BuscarTrabajadorNumDocumento()
        {
            this.dataListado.DataSource = NTrabajador.BuscarTrabajadorNumDocumento(this.textBoxBuscar.Text);
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        private void FrmTrabajador_Load(object sender, EventArgs e)
        {
            this.Mostrar();
            this.Habilitar(false);
            this.Botones();
        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            if (comboBoxBuscar.Text.Equals("APELLIDOS"))
            {
                this.BuscarTrabajadorApellidos();
            }
            else if (comboBoxBuscar.Text.Equals("DOCUMENTO"))
            {
                this.BuscarTrabajadorNumDocumento();
            }
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Opcion;
                Opcion = MessageBox.Show("Realmente desea elimar los registros?", "Sistema de Ventas", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (Opcion == DialogResult.OK)
                {
                    string Codigo;
                    string Rpta = "";

                    foreach (DataGridViewRow row in dataListado.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            Codigo = Convert.ToString(row.Cells[1].Value);
                            Rpta = NTrabajador.Eliminar(Convert.ToInt32(Codigo));
                        }
                    }
                    if (Rpta.Equals("OK"))
                    {
                        this.MensajeOk("Se Eliminó Correctamente el registro");
                    }
                    else
                    {
                        this.MensajeError(Rpta);
                    }
                    this.Mostrar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void checkBoxEliminar_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEliminar.Checked)
            {
                this.dataListado.Columns[0].Visible = true;
            }
            else
            {
                this.dataListado.Columns[0].Visible = false;
            }
        }

        private void buttonNuevo_Click(object sender, EventArgs e)
        {
            this.IsNuevo = true;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();
            this.Habilitar(true);
            this.textBoxNombre.Focus();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string rpta = "";
                if (this.textBoxNombre.Text == string.Empty ||
                    this.textBoxApellidos.Text == string.Empty ||
                    this.textBoxNumDocumento.Text == string.Empty ||
                    this.textBoxDireccion.Text == string.Empty ||
                    this.textBoxUsuario.Text == string.Empty||
                    this.textBoxContrasena.Text == string.Empty||
                    this.comboBoxAcceso.Text == string.Empty)
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcon.SetError(textBoxNombre, "Ingrese un valor");
                    errorIcon.SetError(textBoxApellidos, "Ingrese un valor");
                    errorIcon.SetError(textBoxNumDocumento, "Ingrese un valor");
                    errorIcon.SetError(textBoxDireccion, "Ingrese un valor");
                    errorIcon.SetError(textBoxUsuario, "Ingrese un valor");
                    errorIcon.SetError(textBoxContrasena, "Ingrese un valor");
                    errorIcon.SetError(comboBoxAcceso, "Ingrese un valor");
                }
                else
                {
                    if (this.IsNuevo)
                    {
                        rpta = NTrabajador.Insertar(
                            this.textBoxNombre.Text.Trim().ToUpper(),
                            this.textBoxApellidos.Text.Trim().ToUpper(),
                            this.comboBoxSexo.Text,
                            this.dateTimePickerFechaNacimiento.Value,
                            this.textBoxNumDocumento.Text,
                            this.textBoxDireccion.Text,
                            this.textBoxTelefono.Text,
                            this.comboBoxAcceso.Text,
                            this.textBoxUsuario.Text,
                            this.textBoxContrasena.Text);
                    }
                    else
                    {
                        rpta = NTrabajador.Editar(
                            Convert.ToInt32(this.textBoxIdTrabajador.Text),
                            this.textBoxNombre.Text.Trim().ToUpper(),
                            this.textBoxApellidos.Text.Trim().ToUpper(),
                            this.comboBoxSexo.Text,
                            this.dateTimePickerFechaNacimiento.Value,
                            this.textBoxNumDocumento.Text,
                            this.textBoxDireccion.Text,
                            this.textBoxTelefono.Text,
                            this.comboBoxAcceso.Text,
                            this.textBoxUsuario.Text,
                            this.textBoxContrasena.Text);
                    }

                    if (rpta.Equals("OK"))
                    {
                        if (this.IsNuevo)
                        {
                            this.MensajeOk("Se insertó de forma correcta el registro");
                            errorIcon.Clear();
                        }
                        else
                        {
                            this.MensajeOk("Se actualizó de forma correcta el registro");
                            errorIcon.Clear();
                        }
                    }
                    else
                    {
                        this.MensajeError(rpta);
                    }

                    this.IsNuevo = false;
                    this.IsEditar = false;
                    this.Botones();
                    this.Limpiar();
                    this.Mostrar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
            
        }

        private void buttonEditar_Click(object sender, EventArgs e)
        {
            if (!this.textBoxIdTrabajador.Text.Equals(""))
            {
                this.IsEditar = true;
                this.Botones();
                this.Habilitar(true);
            }
            else
            {
                this.MensajeError("Debe seleccionar primero el registro a modificar");
            }
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.IsNuevo = false;
            this.IsEditar = false;
            this.Botones();
            this.Limpiar();
            this.Habilitar(false);
            textBoxIdTrabajador.Text = string.Empty;
            errorIcon.Clear();
        }

        private void dataListado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataListado.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell checkBoxEliminar =
                    (DataGridViewCheckBoxCell)dataListado.Rows[e.RowIndex].Cells["Eliminar"];
                checkBoxEliminar.Value = !Convert.ToBoolean(checkBoxEliminar.Value);
            }
        }

        private void dataListado_DoubleClick(object sender, EventArgs e)
        {
            this.textBoxIdTrabajador.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["idtrabajador"].Value);
            this.textBoxNombre.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["nombre"].Value);
            this.textBoxApellidos.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["apellidos"].Value);
            this.comboBoxSexo.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["sexo"].Value);
            this.dateTimePickerFechaNacimiento.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["fecha_nacimiento"].Value);
            this.textBoxNumDocumento.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["numero_documento"].Value);
            this.textBoxDireccion.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["direccion"].Value);
            this.textBoxTelefono.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["telefono"].Value);
            this.comboBoxAcceso.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["acceso"].Value);
            this.textBoxUsuario.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["usuario"].Value);
            this.textBoxContrasena.Text = Convert.ToString(this.dataListado.CurrentRow.Cells["contrasena"].Value);

            this.tabControl1.SelectedIndex = 1;
        }

        private void textBoxBuscar_TextChanged(object sender, EventArgs e)
        {
            buttonBuscar_Click(sender, e);
        }
    }
}
