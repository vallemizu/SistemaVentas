﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using CapaDatos;

namespace CapaNegocio
{
    public class NProveedor
    {
        //Método Insertar que llame al método insertar de la clase Proveedor de la capaDatos
        public static string Insertar(string razonSocial, string sectorComercial, 
            string tipoDocumento, string numDocumento, string direccion, string telefono, 
            string email, string url)
        {
            DProveedor Obj = new DProveedor();
            Obj.RazonSocial = razonSocial;
            Obj.SectorComercial = sectorComercial;
            Obj.TipoDocumento = tipoDocumento;
            Obj.NumDocumento = numDocumento;
            Obj.Direccion = direccion;
            Obj.Telefono = telefono;
            Obj.Email = email;
            Obj.Url = url;
            return Obj.Insertar(Obj);
        }

        //Método Editar
        public static string Editar(int idproveedor, string razonSocial, string sectorComercial, 
            string tipoDocumento, string numDocumento, string direccion, string telefono, 
            string email, string url)
        {
            DProveedor Obj = new DProveedor();
            Obj.Idproveedor = idproveedor;
            Obj.RazonSocial = razonSocial;
            Obj.SectorComercial = sectorComercial;
            Obj.TipoDocumento = tipoDocumento;
            Obj.NumDocumento = numDocumento;
            Obj.Direccion = direccion;
            Obj.Telefono = telefono;
            Obj.Email = email;
            Obj.Url = url;
            return Obj.Editar(Obj);
        }

        //Método Eliminar
        public static string Eliminar(int idproveedor)
        {
            DProveedor Obj = new DProveedor();
            Obj.Idproveedor = idproveedor;
            return Obj.Eliminar(Obj);
        }

        //Método Mostrar
        public static DataTable Mostrar()
        {
            return new DProveedor().Mostrar();
        }

        //Método Buscar RazonSocial
        public static DataTable BuscarRazonSocial(string textobuscar)
        {
            DProveedor Obj = new DProveedor();
            Obj.TextoBuscar = textobuscar;
            return Obj.BuscarRazonSocial(Obj);
        }

        //Método Buscar NumDocumento
        public static DataTable BuscarNumDocumento(string textobuscar)
        {
            DProveedor Obj = new DProveedor();
            Obj.TextoBuscar = textobuscar;
            return Obj.BuscarNumDocumento(Obj);
        }
    }
}
