﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaNegocio;

namespace CapaPresentacion.Consultas
{
    public partial class FrmConsultaStockArticulos : Form
    {
        public FrmConsultaStockArticulos()
        {
            InitializeComponent();
        }

        //Método Ocultar columnas
        private void OcultarColumnas()
        {
            this.dataListado.Columns[0].Visible = false;
        }

        //Método Mostrar Registros
        private void Mostrar()
        {
            this.dataListado.DataSource = NArticulo.StockArticulos();
            this.OcultarColumnas();
            labelTotal.Text = "Total de Registros: " + Convert.ToString(dataListado.Rows.Count);
        }

        private void FrmConsultaStockArticulos_Load(object sender, EventArgs e)
        {
            this.Mostrar();
        }
    }
}
